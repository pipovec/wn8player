<?php declare(strict_types=1);


namespace fekar\wn8player;


use fekar\wn8player\Model\ExpectedTankValue;
use fekar\wn8player\Model\PlayersStat;
use fekar\wn8player\Model\VehicleStat;

/**
 * @author    Boris Fekar
 * @createdAt 1. 12. 2021
 * @package   fekar
 */

/**
 * @param PlayersStat[]
 * @param VehicleStat[]
 * @param ExpectedTankValue[]
 */
class WN8
{
    /**
     * @var float
     */
    private float $wn8 = 0.00;

    /**
     * @var array
     */
    private array $missing_tanks = array();

    /**
     * @var PlayersStat
     */
    private PlayersStat $playersStat;

    /**
     * @var array
     */
    private array $vehicleStat;

    /**
     * @var array
     */
    private array $vehicles = array();

    /**
     * @var array
     */
    private array $expectedTankValue;

    /**
     * @var array
     */
    private array $expectedValue;

    public function __construct(PlayersStat $playersStat, array $vehicleStat, array $expectedTankValue)
    {
        $this->playersStat = $playersStat;
        $this->vehicleStat = $vehicleStat;
        $this->expectedTankValue = $expectedTankValue;
    }


    public function calculate(): void
    {
        $expDAMAGE = $expFRAGS = $expSPOT = $expDEF = $expWIN = 0;

        $this->consolidateExpectedValues();
        $this->consolidateVehicleStat();

        /**
         * @var VehicleStat $vehicleStat
         */
        foreach ($this->vehicleStat as $vehicleStat) {

            if (key_exists($vehicleStat->getTankId(), $this->expectedValue)) {

                /**
                 * @var ExpectedTankValue $expectedValue
                 */
                $expectedValue = $this->expectedValue[$vehicleStat->getTankId()];

                // Battles of current tank
                $battles = $vehicleStat->getBattles();

                $expDAMAGE += $expectedValue->getExpDamage() * $battles;
                $expSPOT += $expectedValue->getExpSpot() * $battles;
                $expFRAGS += $expectedValue->getExpFrag() * $battles;
                $expDEF += $expectedValue->getExpDef() * $battles;
                $expWIN += 0.01 * $expectedValue->getExpWinrate() * $battles;

            } else {
                $this->missing_tanks[] = $vehicleStat->getTankId();
            }
        }

        // Calculate WN8
        $rDAMAGE = $this->playersStat->getDamageDealt() / $expDAMAGE;
        $rSPOT = $this->playersStat->getSpotted() / $expSPOT;
        $rFRAG = $this->playersStat->getFrags() / $expFRAGS;
        $rDEF = $this->playersStat->getDef() / $expDEF;
        $rWIN = $this->playersStat->getWins() / $expWIN;

        $rWINc = max(0, ($rWIN - 0.71) / (1 - 0.71));
        $rDAMAGEc = max(0, ($rDAMAGE - 0.22) / (1 - 0.22));
        $rFRAGc = max(0, min($rDAMAGEc + 0.2, ($rFRAG - 0.12) / (1 - 0.12)));
        $rSPOTc = max(0, min($rDAMAGEc + 0.1, ($rSPOT - 0.38) / (1 - 0.38)));
        $rDEFc = max(0, min($rDAMAGEc + 0.1, ($rDEF - 0.10) / (1 - 0.10)));

        $this->wn8 = 980 * $rDAMAGEc + 210 * $rDAMAGEc * $rFRAGc + 155 * $rFRAGc * $rSPOTc + 75 * $rDEFc * $rFRAGc + 145 * MIN(1.8, $rWINc);
    }


    private function consolidateExpectedValues()
    {
        /**
         * @var ExpectedTankValue $value
         */
        foreach ($this->expectedTankValue as $value) {
            $this->expectedValue[$value->getIDNum()] = $value;
        }
    }

    private function consolidateVehicleStat()
    {
        /**
         * @var VehicleStat $value
         */
        foreach ($this->vehicleStat as $value) {
            $this->vehicles[$value->getTankId()] = $value;
        }
    }

    /**
     * @return float
     */
    public function getWn8(): float
    {
        return $this->wn8;
    }

    /**
     * @return array
     */
    public function getMissingTank(): array
    {
        return $this->missing_tanks;
    }
}
