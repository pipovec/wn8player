<?php declare(strict_types=1);


namespace fekar\wn8player\Model;


/**
 * @author    Boris Fekar
 * @createdAt 1. 12. 2021
 * @package   fekar\wn8player\Model
 */
class ExpectedTankValue
{
    private int $IDNum;
    private float $expDef;
    private float $expFrag;
    private float $expSpot;
    private float $expDamage;
    private float $expWinrate;

    /**
     * @return int
     */
    public function getIDNum(): int
    {
        return $this->IDNum;
    }

    /**
     * @param  int $IDNum
     * @return ExpectedTankValue
     */
    public function setIDNum(int $IDNum): ExpectedTankValue
    {
        $this->IDNum = $IDNum;
        return $this;
    }

    /**
     * @return float
     */
    public function getExpDef(): float
    {
        return $this->expDef;
    }

    /**
     * @param  float $expDef
     * @return ExpectedTankValue
     */
    public function setExpDef(float $expDef): ExpectedTankValue
    {
        $this->expDef = $expDef;
        return $this;
    }

    /**
     * @return float
     */
    public function getExpFrag(): float
    {
        return $this->expFrag;
    }

    /**
     * @param  float $expFrag
     * @return ExpectedTankValue
     */
    public function setExpFrag(float $expFrag): ExpectedTankValue
    {
        $this->expFrag = $expFrag;
        return $this;
    }

    /**
     * @return float
     */
    public function getExpSpot(): float
    {
        return $this->expSpot;
    }

    /**
     * @param  float $expSpot
     * @return ExpectedTankValue
     */
    public function setExpSpot(float $expSpot): ExpectedTankValue
    {
        $this->expSpot = $expSpot;
        return $this;
    }

    /**
     * @return float
     */
    public function getExpDamage(): float
    {
        return $this->expDamage;
    }

    /**
     * @param  float $expDamage
     * @return ExpectedTankValue
     */
    public function setExpDamage(float $expDamage): ExpectedTankValue
    {
        $this->expDamage = $expDamage;
        return $this;
    }

    /**
     * @return float
     */
    public function getExpWinrate(): float
    {
        return $this->expWinrate;
    }

    /**
     * @param  float $expWinrate
     * @return ExpectedTankValue
     */
    public function setExpWinrate(float $expWinrate): ExpectedTankValue
    {
        $this->expWinrate = $expWinrate;
        return $this;
    }

}
