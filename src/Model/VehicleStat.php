<?php declare(strict_types=1);


namespace fekar\wn8player\Model;


/**
 * @author    Boris Fekar
 * @createdAt 1. 12. 2021
 * @package   fekar\wn8player\Model
 */
class VehicleStat
{
    private int $tank_id;
    private int $battles;

    /**
     * @return int
     */
    public function getTankId(): int
    {
        return $this->tank_id;
    }

    /**
     * @param  int $tank_id
     * @return VehicleStat
     */
    public function setTankId(int $tank_id): VehicleStat
    {
        $this->tank_id = $tank_id;
        return $this;
    }

    /**
     * @return int
     */
    public function getBattles(): int
    {
        return $this->battles;
    }

    /**
     * @param  int $battles
     * @return VehicleStat
     */
    public function setBattles(int $battles): VehicleStat
    {
        $this->battles = $battles;
        return $this;
    }
}
