<?php declare(strict_types=1);

namespace fekar\wn8player\Model;

/**
 * @author    Boris Fekar
 * @createdAt 1. 12. 2021
 * @package   fekar\wn8player\Model
 */
class PlayersStat
{
    private int $battles;
    private int $damage_dealt;
    private int $spotted;
    private int $frags;
    private int $wins;
    private int $def;

    /**
     * @return int
     */
    public function getBattles(): int
    {
        return $this->battles;
    }

    /**
     * @param  int $battles
     * @return PlayersStat
     */
    public function setBattles(int $battles): PlayersStat
    {
        $this->battles = $battles;
        return $this;
    }

    /**
     * @return int
     */
    public function getDamageDealt(): int
    {
        return $this->damage_dealt;
    }

    /**
     * @param  int $damage_dealt
     * @return PlayersStat
     */
    public function setDamageDealt(int $damage_dealt): PlayersStat
    {
        $this->damage_dealt = $damage_dealt;
        return $this;
    }

    /**
     * @return int
     */
    public function getSpotted(): int
    {
        return $this->spotted;
    }

    /**
     * @param  int $spotted
     * @return PlayersStat
     */
    public function setSpotted(int $spotted): PlayersStat
    {
        $this->spotted = $spotted;
        return $this;
    }

    /**
     * @return int
     */
    public function getFrags(): int
    {
        return $this->frags;
    }

    /**
     * @param  int $frags
     * @return PlayersStat
     */
    public function setFrags(int $frags): PlayersStat
    {
        $this->frags = $frags;
        return $this;
    }

    /**
     * @return int
     */
    public function getWins(): int
    {
        return $this->wins;
    }

    /**
     * @param  int $wins
     * @return PlayersStat
     */
    public function setWins(int $wins): PlayersStat
    {
        $this->wins = $wins;
        return $this;
    }

    /**
     * @return int
     */
    public function getDef(): int
    {
        return $this->def;
    }

    /**
     * @param  int $def
     * @return PlayersStat
     */
    public function setDef(int $def): PlayersStat
    {
        $this->def = $def;
        return $this;
    }
}
